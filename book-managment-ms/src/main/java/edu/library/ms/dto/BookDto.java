package edu.library.ms.dto;

import lombok.Data;

@Data
public class BookDto {
    private Long id;
    private String name ;
    private String janr ;
    private String author ;
    private String publisherId ;
}
