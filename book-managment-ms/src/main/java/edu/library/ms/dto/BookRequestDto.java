package edu.library.ms.dto;

import lombok.Data;

@Data
public class BookRequestDto {
    private String name ;
    private String janr ;
    private String author ;
    private String publisherId ;

}
