package edu.library.ms.repository;

import edu.library.ms.domain.Book;
import edu.library.ms.dto.SearchSpecification;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface BookRepository extends JpaRepository<Book,Long> ,
        JpaSpecificationExecutor<Book> {
    List<Book> findByPublisherId(String id);
}
