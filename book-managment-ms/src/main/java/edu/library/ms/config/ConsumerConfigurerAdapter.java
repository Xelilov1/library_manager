package edu.library.ms.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class ConsumerConfigurerAdapter  extends WebSecurityConfigurerAdapter {
    private final FilterConfigurerAdapter filterConfigurerAdapter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.apply(filterConfigurerAdapter);
        http
                .csrf().disable()//cors
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
                .antMatchers("/books/public-books").permitAll()   //herkese aciqdir
                .and()
                .authorizeRequests()
                .antMatchers("/books/authenticated/**","/books/search/**","/books/{id}","/books/publish/{publish-id}"  ).authenticated()   //mutleq login olmalisan rolun onemi yoxdur
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/books/**").hasAuthority("ROLE_ADMIN");    //mutleq admin olmalidir

        super.configure(http);
    }
}
