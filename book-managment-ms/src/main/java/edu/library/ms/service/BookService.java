package edu.library.ms.service;

import edu.library.ms.dto.BookDto;
import edu.library.ms.dto.BookRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookService {
    List<BookDto> getAllBooks();

    Page<BookDto> search(String query, Pageable pageable);

    BookDto get(Long id);

    List<BookDto> getBookByPublishId(String publishId);

    BookDto create(BookRequestDto requestDto);
}
