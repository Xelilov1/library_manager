package edu.library.ms.service.impl;

import edu.library.ms.domain.Book;
import edu.library.ms.dto.*;
import edu.library.ms.exception.BookNotFoundException;
import edu.library.ms.repository.BookRepository;
import edu.library.ms.service.BookService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final ModelMapper modelMapper=new ModelMapper();
    @Override
    public List<BookDto> getAllBooks() {
        return bookRepository.findAll()
                .stream()
                .map(book -> modelMapper.map(book, BookDto.class))
                .collect(Collectors.toList());
    }

    @Override
   // @Transactional
    public Page<BookDto> search(String query, Pageable pageable) {
        var searchSpec = List.of(searchCriteria("name", query), searchCriteria("janr", query));
        return bookRepository
                .findAll(new SearchSpecification<>(searchSpec, MatchType.OR), pageable)
                .map(book -> {
                    return modelMapper.map(book, BookDto.class);
                });
    }

    @Override
    public BookDto get(Long id) {
        return bookRepository
                .findById(id)
                .map(book -> {
                 return modelMapper.map(book, BookDto.class);
                })
                .orElseThrow(() -> new BookNotFoundException(id));
    }

    @Override
    public List<BookDto> getBookByPublishId(String publishId) {
        return bookRepository.findByPublisherId(publishId).stream()
                .map(book ->
                        modelMapper.map(book,BookDto.class)).collect(Collectors.toList());
    }

    @Override
    public BookDto create(BookRequestDto requestDto) {
        String currentUserName="";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            requestDto.setPublisherId(currentUserName);
        }
        Book book=new Book();
        book.setAuthor(requestDto.getAuthor());
        book.setJanr(requestDto.getJanr());
        book.setPublisherId(requestDto.getPublisherId());
        book.setName(requestDto.getName());
        System.out.println(" fvhfbkdnfk --- "+book);


       Book saved = bookRepository.save(book);

       return modelMapper.map(saved, BookDto.class);
        //return null;
    }

    private SearchCriteria searchCriteria(String key, String value) {
        return SearchCriteria
                .builder()
                .key(key)
                .value(value)
                .operation(SearchOperation.MATCH)
                .build();
    }
}
