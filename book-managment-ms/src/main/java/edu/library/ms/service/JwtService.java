package edu.library.ms.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;

@Slf4j
@Service
@RequiredArgsConstructor
public class JwtService {
    private Key key;


    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode("aGVyIHNleSBiaXIgZ3VuIGdvemxlIG9sYWNhcSBldmVyeXR5aGluZyB3aWxsIGJlIG9rZXkgaGV5YXQgc2V2aW5jZSBnb3psZQ==");
        key = Keys.hmacShaKeyFor(keyBytes);
    }


    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }
}
