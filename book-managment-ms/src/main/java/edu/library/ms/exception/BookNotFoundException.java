package edu.library.ms.exception;

public class BookNotFoundException extends NotFoundException {

    public static final String MESSAGE = "Course with id %s not found";
    private static final long serialVersionUID = 5843213248811L;

    public BookNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }

}