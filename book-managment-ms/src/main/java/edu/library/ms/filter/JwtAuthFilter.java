package edu.library.ms.filter;

import edu.library.ms.service.JwtService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class JwtAuthFilter extends OncePerRequestFilter {
    private static final String AUTH_HEADER = "Authorization";
    private static final String BEARER = "Bearer";
    private static final String ROLE_CLAIM = "ROLE";

    private final JwtService jwtService;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
       // log.info("Our filter is in action {}", removeBearerHeader(request.getHeader(AUTH_HEADER)));

  if(request.getHeader(AUTH_HEADER)!=null){
      Claims claims = jwtService.parseToken(removeBearerHeader(request.getHeader(AUTH_HEADER)));
      Authentication authentication = getAuthenticationBearer(claims);
      SecurityContextHolder.getContext().setAuthentication(authentication);
  }
     //   log.info("CLASIMS {}, " + claims);

        filterChain.doFilter(request, response);
    }


    private String removeBearerHeader(String header) {
        if (header.startsWith(BEARER)) {
            return header.substring(BEARER.length() + 1);
        }
        throw new RuntimeException("It is not bearer token");
    }

    private Authentication getAuthenticationBearer(Claims claims) {
        List<?> roles = claims.get(ROLE_CLAIM, List.class);

        List<GrantedAuthority> authorityList = new ArrayList<>();
        for (int i = 0; i < roles.size(); i++) {
            String role = roles.get(i).toString().substring(18, roles.get(i).toString().length() - 1);
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(role);
            authorityList.add(simpleGrantedAuthority);
        }

        log.info("authortyList {}", authorityList);
//        return new UsernamePasswordAuthenticationToken(claims.getSubject(), "", authorityList);
        return new UsernamePasswordAuthenticationToken(claims.getSubject(), "", authorityList);
    }
}
