package edu.library.ms.controller;

import edu.library.ms.dto.BookDto;
import edu.library.ms.dto.BookRequestDto;
import edu.library.ms.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    @GetMapping("/public-books")
    public ResponseEntity publishBooks(){
        return new ResponseEntity(bookService.getAllBooks(), HttpStatus.CREATED);
    }

    @GetMapping("/search")
    public ResponseEntity<Page<BookDto>> search(@RequestParam(required = false, defaultValue = "") String query,
                                                Pageable pageable) {
        return ResponseEntity.ok(bookService.search(query, pageable));
    }


    @GetMapping("/{id}")
    public ResponseEntity<BookDto> getById(@PathVariable Long id) {
        return ResponseEntity.ok(bookService.get(id));
    }


    @GetMapping("/publish/{publish-id}")
    public ResponseEntity<List<BookDto>> getBooksByPublishId(@PathVariable(name = "publish-id") String publishId) {
        return ResponseEntity.ok(bookService.getBookByPublishId(publishId));
    }


    @PostMapping
    public void addBook(@RequestBody BookRequestDto bookRequestDto){
        bookService.create(bookRequestDto);
    }






}
