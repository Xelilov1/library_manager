package edu.library.ms.controller;
import edu.library.ms.dto.HelloDto;
import edu.library.ms.dto.JwtToken;
import edu.library.ms.dto.SignInDto;
import edu.library.ms.dto.SignUpDto;
import edu.library.ms.jwt.JwtService;
import edu.library.ms.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/secure")
public class SecureController {

    private  final UserService userService;
    private  final JwtService jwtService;

    @GetMapping("/hello")
    public HelloDto helloWorld(){
        return new HelloDto("Bu methodu cagirmaq ucun login olmaq lazim deyil.Cunki permitAll demisik");
    }

    @PostMapping("sign-up")
    public HelloDto signUp(@RequestBody SignUpDto signUpDto){
        System.out.println(" SignUpDto "+signUpDto);
        userService.signUp(signUpDto);
        return new HelloDto("Bu methodu cagirmaq ucun login olmaq lazim deyil.Cunki permitAll demisik");
    }

    @PostMapping("/sign-in")
    public JwtToken signUp(@RequestBody SignInDto signInDto){
        System.out.println(" SignInDto "+signInDto);
       return userService.signIn(signInDto);
    }

    @GetMapping("/authenticated")
    public HelloDto afterLogin(Principal principal){
        log.info("the user is {} ",principal);
        return new HelloDto("Bu methodu cagirmaq ucun mutleq login olmalisan!. Rolun onemi yoxdur");
    }

    @GetMapping("/user")
    public HelloDto user(){
        return new HelloDto("Bu methodu cagirmaq ucun mutleq USER ve ya ADMIN  olmalisan!.");
    }
    @GetMapping("/admin")
    public HelloDto admin(){
        return new HelloDto("Bu methodu cagirmaq ucun mutleq  ADMIN  olmalisan!.");
    }

}
