package edu.library.ms.controller;

import edu.library.ms.dto.TestDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {
    @GetMapping
    public TestDto test(){
        TestDto t=new TestDto();
        t.setMessage("Mesajin var!!");
        return t;
    }
}
