package edu.library.ms.service;

import edu.library.ms.dto.JwtToken;
import edu.library.ms.dto.SignInDto;
import edu.library.ms.dto.SignUpDto;

public interface UserService {
    void signUp(SignUpDto signUpDto);
    JwtToken signIn(SignInDto signInDto);
}
