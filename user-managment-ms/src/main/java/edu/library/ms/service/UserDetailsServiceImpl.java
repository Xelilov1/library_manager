package edu.library.ms.service;

import edu.library.ms.config.PasswordEncodeConfig;
import edu.library.ms.domain.User;
import edu.library.ms.domain.UserAuthority;
import edu.library.ms.dto.JwtToken;
import edu.library.ms.dto.SignInDto;
import edu.library.ms.dto.SignUpDto;
import edu.library.ms.jwt.JwtService;
import edu.library.ms.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl  implements UserDetailsService ,UserService{

    private final UserRepository userRepository;
    private final JwtService jwtService;
    private final PasswordEncodeConfig passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Loading by username {}",username);
       // log.info(jwtService.issueToken(userRepository.findByUsername(username).get(), Duration.ofDays(1)));
      return  userRepository.findByUsername(username)
              .orElseThrow(()-> new UsernameNotFoundException("User not found with "+username));
    }
//    @Bean
//    public BCryptPasswordEncoder passwordEncoder(){
//        return new BCryptPasswordEncoder();
//    }

    @Override
    public void signUp(SignUpDto signUpDto) {
        User user=new User();
        Set<UserAuthority> userAuthorities=new HashSet<>();
        user.setUsername(signUpDto.getUsername());
       // user.setPassword(signUpDto.getPassword());
        user.setPassword(passwordEncoder.passwordEncoder().encode(signUpDto.getPassword()));
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        Set<String> set=signUpDto.getAuthorities();
        for (String s:set) {
            UserAuthority userAuthority=new UserAuthority();
            userAuthority.setAuthority(s);
            userAuthorities.add(userAuthority);
        }
        user.setAuthorities(userAuthorities);
        userRepository.save(user);
    }


    public JwtToken signIn(SignInDto signInDto) {
        BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

        User user= (User) loadUserByUsername(signInDto.getUsername());
        String hashedPassword = "";
        boolean isPasswordMatched=false;
        if(user!=null){
            hashedPassword=user.getPassword();
             isPasswordMatched= bcryptEncoder.matches(signInDto.getPassword(), hashedPassword);
        }
        if(isPasswordMatched){
            System.out.println("use -> "+user);
            String jwt=null;
            jwt= jwtService.issueToken(user,Duration.ofDays(1));
            return new  JwtToken(jwt);
        }
        throw new RuntimeException("Username ve ya password is wrong");

    }
}
