package edu.library.ms.dto;

import lombok.Data;

@Data
public class TestDto {
    private String message;
}
