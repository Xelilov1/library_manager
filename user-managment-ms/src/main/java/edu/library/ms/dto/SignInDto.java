package edu.library.ms.dto;

import lombok.Data;

@Data
public class SignInDto {
    private String username;
    private String password;
}
