package edu.library.ms.dto;

import edu.library.ms.domain.UserAuthority;
import lombok.Data;

import java.util.Set;

@Data
public class SignUpDto {
    private String username;
    private String password;
    Set<String> authorities;
}
