package edu.library.ms.domain;

import lombok.Data;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;

    private String password;

    public boolean accountNonExpired;

    public boolean accountNonLocked;

    public boolean credentialsNonExpired;

    public boolean enabled;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<UserAuthority> authorities;


}

