package edu.library.ms.jwt;

import edu.library.ms.domain.User;
import edu.library.ms.dto.SignInDto;
import edu.library.ms.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class JwtService {

    private Key key;


    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode("aGVyIHNleSBiaXIgZ3VuIGdvemxlIG9sYWNhcSBldmVyeXR5aGluZyB3aWxsIGJlIG9rZXkgaGV5YXQgc2V2aW5jZSBnb3psZQ==");
        key = Keys.hmacShaKeyFor(keyBytes);
    }


    public String issueToken(User user, Duration duration) {
        log.info("Issue JWT token to {} for {}", user, duration);
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(user.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(duration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS512)
                .addClaims(Map.of("ROLE", user.getAuthorities()));
//        addClaimsSets(jwtBuilder, authentication);
//        addClaims(jwtBuilder, authentication);
        return jwtBuilder.compact();
    }


    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }



}

