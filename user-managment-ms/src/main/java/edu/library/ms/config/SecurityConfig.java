package edu.library.ms.config;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()//cors
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
                .antMatchers("/secure/hello/**","/secure/sign-up/**","/secure/sign-in/**").permitAll()   //herkese aciqdir
                .and()
                .authorizeRequests()
                .antMatchers("/secure/authenticated/**").authenticated()   //mutleq login olmalisan rolun onemi yoxdur
                .and()
                .authorizeRequests()
                .antMatchers("/secure/user").hasAnyRole("USER", "ADMIN") //mutleq user ve ya admin rolu olmalidir
                .and()
                .authorizeRequests()
                .antMatchers("/secure/admin").hasAnyRole("ADMIN");    //mutleq admin olmalidir
//                .and().authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/secure/same").permitAll()   // eger eyni adli api varsa ve siz bunlara ferqli permission vermek ideyirsizse
//                .and()                                                                //bu cur methdod tipi ile ferqlendire bilersiniz .
//                .authorizeRequests()                                                  //Her 2 si same adindadir amma ferqli methdo tipinde
//                .antMatchers(HttpMethod.POST, "/secure/same").authenticated();


        super.configure(http);
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("ferid").password("{noop}ferid").roles("USER")
//                .and()
//                .withUser("admin").password("{noop}admin").roles("ADMIN");
//    }


}

